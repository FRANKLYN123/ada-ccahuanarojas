package a2ojsec;

import java.util.*;

public class ws{
	/*
	
1
20 3
3 4 6 8
2 5 10
4 1 3 5 5

	 */
	static int[][] gp; 
	static int[][] memo;
	
	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		
		int n = sc.nextInt();
		int m;
		int c;
		
		int[] temp;
		int[] tempMemo;
		int d;

		while((n--)!=0){
			m = sc.nextInt();
			c = sc.nextInt();
			
			gp = new int[c][1];
			memo = new int[210][25];
			
			for (int i = 0; i < 210; i++)
				 for (int j = 0; j < 25; j++)
				 memo[i][j] = -1;
			
			for(int i=0;i<c;i++){
				d = sc.nextInt();
				temp = new int[d];
				
				for(int j=0;j<d;j++){
					temp[j] = sc.nextInt();
				
				}	
				gp[i] = temp;
				
			}
			
			
			int rest = dp(m,0,gp);
			if(m-rest>=0)
				System.out.println(m-rest);
			else
				System.out.println("no solution");
			
			
		}
				
			
	

	}


	public static void print(int[][] a){
					
		for(int i=0;i<a.length;i++){
			for(int j=0;j<a[i].length;j++){
				System.out.print(a[i][j]+" ");

			}		
			System.out.println();
		}					
	}

	public static int dp(int m,int c,int[][] gp){
		
		
		if(m<0)
			return Integer.MAX_VALUE;
		if(c==gp.length)
			return m;
		if(memo[m][c]!=-1)
			return memo[m][c];
		
		int min = Integer.MAX_VALUE;
	
		for(int i=0;i<gp[c].length;i++){
			min = Math.min(min,dp((m-gp[c][i]),c+1,gp));	
			
		}
		memo[m][c] = min;
		return min;
		
	}

}
