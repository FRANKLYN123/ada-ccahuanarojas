package a2ojsec;
import java.util.*;

public class SuperSale {
	
	static int[][] ar;
	static int[] object,person,price;
	static int n_of_o,n_of_p;
	static int p;
	static int a = 0;
	
	public static void main(String[] args){
		
		Scanner in = new Scanner(System.in);
		
		int n,i,j;
		n = in.nextInt();

		for(i=1;i<=n;i++)
		{
			ar = new int [1001][31];
			object = new int[1001];
			person = new int[101];
			price = new int[1001];
			n_of_o = in.nextInt();

			for(j=0;j<n_of_o;j++)
			{
				price[j] = in.nextInt();
				object[j] = in.nextInt();
			}
			n_of_p = in.nextInt();

			for(j=0;j<n_of_p;j++)
			{
				person[j] = in.nextInt();
			}
			int total=0,temp;


            int x = 0;
            int y = 0;

			for(j=0;j<n_of_p;j++)
			{
				p=j;
				for(x=0;x<1001;x++){
                    for(y=0;y<31;y++){
                        ar[x][y] = 1;
                    }
				}
				temp=dp(0,0);
				total=total+temp;
			}
			System.out.println(total);
		}
	}
	
	
	
	
	public static int dp(int o,int weight)
	{
			if(o>=n_of_o)return 0;
			if(ar[o][weight]!=1)
			{
				return ar[o][weight];
			}
			int cost1=0,cost2=0;
			if(object[o]<=person[p]-weight)
			{
				cost1=price[o]+dp(o+1,weight+object[o]);
			}
			cost2=dp(o+1,weight);
			if(cost1>cost2)
			{
				ar[o][weight]=cost1;
				return cost1;
			}
			ar[o][weight]=cost2;
			return ar[o][weight];
	}
}
