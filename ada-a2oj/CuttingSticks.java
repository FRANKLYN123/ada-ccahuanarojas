package a2ojsec;

import java.util.Scanner;

public class CuttingSticks {

	static int[] c ;
	static int[][] dp ;
	
	public static void main(String[] args){
		
		Scanner in = new Scanner(System.in);
		int l, n;
		
		while (true) {
			
			l = in.nextInt();
			
			if(l==0)
				break;
			
	        n = in.nextInt();
	        
	        c = new int[53];
	        dp = new int[53][53];
	        
	        int i = 0;
	        for (i = 1; i <= n; i++ )
	            c[i] = in.nextInt();

	        c [0] = 0;
	        c [n + 1] = l;

	        for (int x = 0; x < 53; x++ ) {
		        for (int y = 0; y < 53; y++ )
		            dp [x] [y] = -1;
		    }

	        System.out.println("The minimum cutting is "+ bktk (0, n + 1)+".");
	    }

	    
	}
	public static int bktk (int s, int e)
	{
	    if ( s + 1 == e ) return 0;

	    if ( dp [s] [e] != -1 )
	        return dp [s] [e];

	    int cost = 0;
	    int minimum = Integer.MAX_VALUE;
	    int i = s;
	    for (i = s + 1; i < e; i++ ) {
	        cost = bktk (s, i) + bktk (i, e) + c [e] - c [s];
	        if ( cost < minimum ) minimum = cost;
	    }

	    return dp [s] [e] = minimum;
	}
	
	
}
