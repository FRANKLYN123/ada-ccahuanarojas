public class MultiplicacionMatrices{

    public static void main(String[] args){
        
      	int [][] a = {{1,2},{3,4},{3,4}};
      	int [][] b = {{1,2,3},{3,4,3}};

      	int [][] resolved = new int [a.length][b[0].length];
      
        for(int i=0;i<a.length;i++){
            for(int j=0;j<a.length;j++){
                for(int k=0;k<a[0].length;k++){
					resolved[i][j] += a[i][k]*b[k][j];
                }
            }
        
        }
        
     	imprimir(resolved);
     
        
    }

 
    public static void imprimir(int[][] a){
      
      for(int i=0;i<a.length;i++){
          
            for(int j=0;j<a[i].length;j++){
                System.out.print(a[i][j]+ " ");
    
            }
            System.out.println();
            
            
        }
      
    }

}