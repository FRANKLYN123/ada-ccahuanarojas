public class Strassen{   
    public static void main(String[]args){   
        
        int [][] A = {{1,2},{3,4}};   
        int [][] B = {{1,2},{3,4}}; 
        
        
        System.out.println("Multiplicacion de matrices Strassen:");   
        strassen(A,B);
   
        }   

        public static int[][] sum(int[][]A,int [][]B){   
            int[][]C = new int[A.length][B.length];   
            
            for(int i = 0; i <A.length;i++){   
                for(int j = 0; j < B.length;j++){   
                    C[i][j] = A[i][j] + B[i][j];   
                }   
            }   
            return C;   
        }  
 
        public static int[][] rest(int[][]A,int [][]B){   
            
            int[][]C = new int[A.length][B.length];   
            for(int i = 0; i <A.length;i++){   
                for(int j = 0; j < B.length;j++){   
                    C[i][j] = A[i][j] - B[i][j];   
                    }   
                }   
            return C;   
        }   

        public static int[][] mult(int[][] A,int[][] B){   

            int[][] C = new int[A.length][A.length];   

            for(int i = 0; i < C.length; i++){   
                for(int j = 0; j < C.length;j++){   
                    for(int k = 0; k < C.length; k++){   
                        C[i][j] += A[i][k] * B[k][j];   
                    }   
                }   
            }
   
            return C;
   
        }
   
        public static void print(int[][] array){   

            for(int i = 0; i < array.length; i++){   
                for(int j= 0; j < array.length; j++){   
                    System.out.print(array[i][j] + " ");   
                }   
                System.out.println();   
            }   

        }
   
        public static void strassen(int[][]A, int[][]B){   
            
            int middle = A.length/2;
            
            // {{a,b},{c,d}} of A  
            int[][] A11 = new int[middle][middle];   
            int[][] A12 = new int[middle][middle];   
            int[][] A21 = new int[middle][middle];   
            int[][] A22 = new int[middle][middle];

            // {{e,f},{g,h}} of B   
            int[][] B11 = new int[middle][middle];   
            int[][] B12 = new int[middle][middle];   
            int[][] B21 = new int[middle][middle];   
            int[][] B22 = new int[middle][middle];
   
            for(int i = 0; i < middle; i++){   
                for(int j = 0; j < middle; j++){   
                    A11[i][j] = A[i][j];   
                    B11[i][j] = B[i][j];   
                    A12[i][j] = A[i][middle+j];   
                    B12[i][j] = B[i][middle+j];   
                    A21[i][j] = A[middle+i][j];   
                    B21[i][j] = B[middle+i][j];   
                    A22[i][j] = A[middle+i][middle+j];   
                    B22[i][j] = B[middle+i][middle+j];   
                }   
            }

   
            int[][] P1 = mult(A11,(rest(B12,B22)));   
            int[][] P2 = mult((sum(A11,A12)),B22);   
            int[][] P3 = mult(B11,(sum(B21,B22)));   
            int[][] P4 = mult(A22,(rest(B21,B11)));   
            int[][] P5 = mult((sum(A11,A22)),sum(B11,B22));   
            int[][] P6 = mult((rest(A12,A22)),(sum(B21,B22)));   
            int[][] P7 = mult((rest(A11,A21)),(sum(B11,B12)));   

            int[][] C11 = sum((sum(P5,rest(P4,P2))),P6);   
            int[][] C12 = sum(P1,P2);   
            int[][] C21 = sum(P3,P4);   
            int[][] C22 = rest(sum(P5,P1),sum(P3,P7));
            
            int[][] C = new int[A.length][A.length];   

            for(int i = 0; i<middle; i++){   
                for(int j= 0; j<middle;j++){   
                    C[i][j] = C11[i][j];   
                    C[i][middle+j] = C12[i][j];   
                    C[middle+i][j] = C21[i][j];   
                    C[middle+i][middle+j] = C22[i][j];   
                }   
            }   
            print(C);   
        }   
}  
