import java.util.Scanner;

public class StatisticAnalysis {

    long [] values = null;
    double mean = 0;
    double median = 0;
    double mode = 0;
    double variance = 0;
    double standardDeviation = 0;
    
    public StatisticAnalysis(long[] values){
        super();
        this.values = values;
        mean = mean(values);
        variance = variance(values);
        standardDeviation = standardDeviation();
    }
	


    public static void sort(double numberValues[]){

    }

    public double mean(long values[]){

        double sum = 0;
        for(int i=0;i<values.length;i++){
            sum += values[i];
        }
        
        return (double) sum/values.length;
    }

    public static void median(double numberValues[]){

    }

    public static void mode(double numberValues[]){

    }

    public double standardDeviation(){
        return (double) Math.sqrt(variance);
    }

    public double variance(long [] values){


        double sum = 0;
        
        for(int i=0;i<values.length;i++){
            sum += (double) Math.pow(values[i]-mean,2);    
        }
    
        return (double) sum/values.length;
    }
/*
    public static void main(String args[]){
        long [] values = {435, 309, 257, 367, 409, 370, 305, 351, 369, 277};
        StatisticAnalysis s = new StatisticAnalysis(values);
        
        double v = s.standardDeviation;
        System.out.println("variance:1 "+v);
    }
*/
}