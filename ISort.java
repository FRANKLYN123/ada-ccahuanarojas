import java.util.*;
public class ISort{

  public static int[] sort(int[] a){

    for(int i=1;i<a.length;i++){
      int k=a[i];
      int j=i-1;
      while(j>=0 && a[j]>k){
        a[j+1]=a[j];
        j--;
      }
      a[j+1]=k;
    }  
    return a;
  }

  public static void mergeSort(int[] a,int p,int r){
    if(p<r){
      int q =(int) (p+r)/2;
      mergeSort(a,p,q);
      mergeSort(a,q+1,r);
      merge(a,p,q,r);
    }

    
  }
  
  public static  void merge(int[] a,int p,int q,int r){
    int n1 = q-p+1;
    int n2 = r-q;

    int[] le = new int[n1+1];
    int[] ri = new int[n2+1];
    
    int i=0;
    int j=0;
    for(i=0;i<n1;i++)
      le[i] = a[p+i];
    for(j=0;j<n2;j++)
      ri[j] = a[q+j+1];
   

    le[n1] = 100;
    ri[n2] = 100;
    
    i=0;
    j=0;

    for(int k=p;k<=r;k++){
      if(le[i]<=ri[j]){
	a[k]=le[i];
	i++;
      }else{
        a[k]=ri[j];
	j++;
      }
    }

  }

  public static void main(String []args){
   
    if(args.length==0){
        System.err.println("Usage: $java ISort <array_max_size> <array_size_interval>");
        System.exit(1);
    }   
    
    int max_size = Integer.parseInt(args[0]);
    int interval = Integer.parseInt(args[1]);
    int num_repeat = 1000;
 
    for(int sizeArray=1;sizeArray<=max_size;sizeArray += interval){
      
 
        long[] times = new long[num_repeat];
        double stddv = 0;
        double mean = 0;

        do{

            for(int i = 0; i < num_repeat; i++){
	
                int[] source = ArrayGenerator.generateArray(sizeArray);

                long startTime = System.nanoTime();
                mergeSort(source,0,source.length-1);
                long estimatedTime = System.nanoTime() - startTime;
                times[i] = estimatedTime;

            }
            StatisticAnalysis temp = new StatisticAnalysis(times);
            stddv = temp.standardDeviation;
            mean = temp.mean;


        }while(stddv>=5);

        System.out.println(sizeArray+" "+mean); 

    }
  }

  public static void imprimir(long[] a){
    System.out.println();
    for(int i=0;i<a.length;i++){
       System.out.print(a[i]+" ");
    }
    System.out.println();
  }
}