public class BinarySearch{

    public static void main(String[] args){

        int[] array = {1,1,0,1,0};
        
        //int index = binary_search(array,2);

        System.out.println(palindrome(array));
    }
    
    public static int binary_search(int[] array,int x){
        
        
        int left = 0;
        int right = array.length;
        int p = 0;
 
        do{
            p = (int) (left+right)/2;
            if(x>array[p])
                left = p+1;
            else
                right = p-1;  
            
        }while(!(array[p]==x || left>right));

        if(array[p] != x)
            return -1;

        return p+1; 
    }

}
