public class Palindrome{

    public static void main(String[] args){

        int[] array = {1,1,0,1,0};
        
        //int index = binary_search(array,2);

        System.out.println(palindrome(array));
    }



    public static boolean palindrome(int[] array){

        int left = 0;
        int right = array.length-1;
        
        while(left<right && array[left]==array[right]){
            left++;
            right--;
        }
            
        if(left>=right)
            return true;
        else
            return false;
    }

}
