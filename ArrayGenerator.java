import java.util.*;
import java.io.*;
public class ArrayGenerator{
 
  public static int[] generateArray(int size){
  
    int[] array = new int[size];
    List<Integer> l = new ArrayList<Integer>();
    for(int i = 0; i < size; i++)
      l.add(i+1);
    for(int i = 0; i < size; i++){
      array[i] = size-i;
    }
    return array;
  }
  public static void print(PrintStream out, int[] a){
    int i;
    for(i = 0; i < a.length - 1; i++)
      out.print(a[i] + " ");
    out.println(a[i]);
  }


  public static void main(String[] args){
    if(args.length == 0){
      System.err.println("Usage: $java ArrayGenerator <array max size> <array size interval>");
      System.exit(1);
    }

    int max_size = Integer.parseInt(args[0]);
    int interval = Integer.parseInt(args[1]);
    int num_repeat = 100;   

    for(int i = 1; i <= max_size; i+=interval){
        System.out.println(i+" "+num_repeat);
        for(int j=1;j<=num_repeat;j++){
            print(System.out,generateArray(i));         
        }
    }
  }

}

